

## 📋 Índice

- [Sobre](#-Sobre)
- [Tecnologias utilizadas](#-Tecnologias-utilizadas)
- [Como executar o projeto](#-Como-executar-o-projeto)
- [Preview](#-Preview)

---

## 🖥 Preview 

<p align="center">
  <img src="https://ik.imagekit.io/devpedropereira/PrintOstenMoove_8iicISIJf.PNG?ik-sdk-version=javascript-1.4.3&updatedAt=1658741777374" width="80%" >
</p>

---

## 📖 Sobre 

Este é um projeto desenvolvido para demonstrar minhas habilidades como desenvolvedor de software.

O projeto foi dividido em duas partes, uma parte de back-end e outra parte de front-end.
back-end, onde desenvolvi uma aplicação de cadastro de empresas.


--- 

## 🚀 Tecnologias utilizadas

O projeto está desenvolvido utilizando as seguintes tecnologias:

- HTML
- CSS
- JavaScript
- TypeScript
- React
- Nextjs
- Node.js 
- Nunjucks 
- SQLite
- Typeorm

- Tailwindcss
- Phosphor-react

--- 

## ⌨ Como executar o projeto





```bash
# Primeiramente, clone os repositórios

git clone https://gitlab.com/devpedrospereira/desafio-ostem-moove.git

# Entrar no diretório
cd desafio-ostem-moove-main

# Dentro do repositórios clonado, existe dois diretórios. Vamos começar com o Back-end:

#Back-end

# Entrar no diretório
cd osten-moove-backend

# Baixar as dependências
npm i

# Executar o servidor
npm run dev

# Parabéns! Sua API e Banco de dados estão funcionando. Para confirmar, acesse:

`http://localhost:3333/company/list`


# Front-end

# Retorne para a raiz de nosso projeto (./desafio-ostem-moove-main)

# Entrar no diretório
cd osten-moove-frontend

# Baixar as dependências
npm i

# Executar o servidor
npm run dev

# Mais uma vez, Parabéns! Seu front-end esta funcionando, acesse:

`http://localhost:3000/`

```

---


Desenvolvido com 💜 por Dev Pedro Pereira
